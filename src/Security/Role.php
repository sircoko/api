<?php

namespace App\Security;

abstract class Role
{
    public const ROLE_SUPERADMIN = 'ROLE_SUPERADMIN';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';
    public const ROLE_USER = 'ROLE_USER';
}
